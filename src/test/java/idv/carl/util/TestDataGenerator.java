package idv.carl.util;

import java.util.ArrayList;
import java.util.List;

import idv.carl.model.User;
import rx.Observable;

/**
 * @author Carl Lu
 */
public class TestDataGenerator {

    private final static Integer DEFAULT_MALE_USER_COUNT = 5000;
    private final static Integer DEFAULT_FEMALE_USER_COUNT = 5000;

    public static List<User> getMassiveUsers(Integer maleUserCount, Integer femaleUserCount) {
        maleUserCount = maleUserCount == null ? DEFAULT_MALE_USER_COUNT : maleUserCount;
        femaleUserCount = femaleUserCount == null ? DEFAULT_FEMALE_USER_COUNT : femaleUserCount;

        List<User> users = new ArrayList<User>();
        users.addAll(maleUsers(maleUserCount));
        users.addAll(femaleUsers(femaleUserCount));
        return users;
    }

    private static List<User> maleUsers(int maleUserCount) {
        List<User> users = new ArrayList<User>();
        Observable.range(1, maleUserCount).forEach(i -> {
            User user = User.getMaleInstance();
            user.initRandomly(i);
            users.add(user);
        });
        return users;
    }

    private static List<User> femaleUsers(int femaleUserCount) {
        List<User> users = new ArrayList<User>();
        Observable.range(1, femaleUserCount).forEach(i -> {
            User user = User.getFemaleInstance();
            user.initRandomly(i);
            users.add(user);
        });
        return users;
    }

}
