package idv.carl;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import idv.carl.model.ViewModel;
import idv.carl.service.ApiService;
import rx.Observable;
import rx.observers.TestSubscriber;

/**
 * @author Carl Lu
 */
public class ViewModelTest {

    @Mock
    ApiService apiServiceMock;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void getCountriesObservable_FiltersOutAtlantis() throws Exception {
        List<String> inputCountries = new ArrayList<>();
        inputCountries.add("USA");
        inputCountries.add("China");
        inputCountries.add("Atlantis");

        Observable<List<String>> observableMock = Observable.just(inputCountries);

        doReturn(observableMock).when(apiServiceMock).getCountries();

        List<String> expectedCountries = new ArrayList<>();
        expectedCountries.add("USA");
        expectedCountries.add("China");

        ViewModel viewModel = new ViewModel(apiServiceMock);
        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        viewModel.getCountriesObservable().subscribe(testSubscriber);
        testSubscriber.assertReceivedOnNext(expectedCountries);
    }

    @Test
    public void getCountriesObservable_StartsRequestWhenSubscribedTo() throws Exception {
        // Returns an Observable that never sends any items or notifications to an Observer.
        Observable<List<String>> observableMock = Observable.never();

        doReturn(observableMock).when(apiServiceMock).getCountries();

        ViewModel viewModel = new ViewModel(apiServiceMock);
        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        viewModel.getCountriesObservable().subscribe(testSubscriber);

        assertTrue(viewModel.isRequesting());
    }

    @Test
    public void getCountriesObservable_FinishesRequestWhenCompleted() throws Exception {
        // Returns an Observable that emits no items to the Observer and immediately invokes its onCompleted method.
        Observable<List<String>> observableMock = Observable.empty();

        doReturn(observableMock).when(apiServiceMock).getCountries();

        ViewModel viewModel = new ViewModel(apiServiceMock);
        TestSubscriber<String> testSubscriber = new TestSubscriber<>();

        viewModel.getCountriesObservable().subscribe(testSubscriber);

        assertFalse(viewModel.isRequesting());
    }

}
