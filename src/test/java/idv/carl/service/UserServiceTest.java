package idv.carl.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import idv.carl.model.User;
import idv.carl.util.TestDataGenerator;

/**
 * @author Carl Lu
 */
public class UserServiceTest {

    private final UserService userService = new UserServiceImpl();
    private final UserService userServiceRx = new UserServiceRxImpl();

    @Test
    public void getTopTenFemaleAgeTest() {
        System.out.println("getTopTenFemaleAgeTest");
        Integer maleUserCount = 9000;
        Integer femaleUserCount = 1000;
        List<User> users = TestDataGenerator.getMassiveUsers(maleUserCount, femaleUserCount);

        long start = System.currentTimeMillis();
        List<Integer> result = userService.getFemalAgeList(users, 10);
        long end = System.currentTimeMillis();
        long executeWithoutRx = end - start;

        start = System.currentTimeMillis();
        List<Integer> resultRx = userServiceRx.getFemalAgeList(users, 10);
        end = System.currentTimeMillis();
        long executeWithRx = end - start;

        assertTrue(result.containsAll(resultRx));

        System.out.println("Execution time without Rx: " + executeWithoutRx);
        System.out.println("Execution time with Rx: " + executeWithRx);
        System.out.println(System.lineSeparator());
    }

    @Test
    public void getTopTenFemaleTest() {
        System.out.println("getTopTenFemaleTest");
        Integer maleUserCount = 9000;
        Integer femaleUserCount = 1000;
        List<User> users = TestDataGenerator.getMassiveUsers(maleUserCount, femaleUserCount);

        long start = System.currentTimeMillis();
        List<User> result = userService.getFemaleList(users, 10);
        long end = System.currentTimeMillis();
        long executeWithoutRx = end - start;

        start = System.currentTimeMillis();
        List<User> resultRx = userServiceRx.getFemaleList(users, 10);
        end = System.currentTimeMillis();
        long executeWithRx = end - start;

        assertTrue(result.containsAll(resultRx));

        System.out.println("Execution time without Rx: " + executeWithoutRx);
        System.out.println("Execution time with Rx: " + executeWithRx);
        System.out.println(System.lineSeparator());
    }

    @Test
    public void getFemaleAgeListTest() {
        System.out.println("getFemaleAgeListTest");
        Integer maleUserCount = 9000;
        Integer femaleUserCount = 1000;
        List<User> users = TestDataGenerator.getMassiveUsers(maleUserCount, femaleUserCount);

        long start = System.currentTimeMillis();
        List<Integer> result = userService.getFemaleAgeList(users);
        long end = System.currentTimeMillis();
        long executeWithoutRx = end - start;

        start = System.currentTimeMillis();
        List<Integer> resultRx = userServiceRx.getFemaleAgeList(users);
        end = System.currentTimeMillis();
        long executeWithRx = end - start;

        assertTrue(result.containsAll(resultRx));

        System.out.println("Execution time without Rx: " + executeWithoutRx);
        System.out.println("Execution time with Rx: " + executeWithRx);
        System.out.println(System.lineSeparator());
    }

}
