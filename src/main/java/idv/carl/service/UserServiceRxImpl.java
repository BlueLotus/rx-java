package idv.carl.service;

import java.util.List;

import idv.carl.model.Gender;
import idv.carl.model.User;
import rx.Observable;

/**
 * @author Carl Lu
 */
public class UserServiceRxImpl implements UserService {

    public Observable<Integer> getAgeObjects(List<User> users) {
        return Observable.from(users).map(user -> user.getAge());
    }

    public Observable<User> getFemaleObjects(List<User> users) {
        return Observable.from(users).filter(user -> user.getGender() == Gender.FEMALE);
    }

    public Observable<User> getFemaleObjects(List<User> users, int limit) {
        return getFemaleObjects(users).take(limit);
    }

    public Observable<Integer> getFemaleAgeObjects(List<User> users) {
        return Observable.from(users).filter(user -> user.getGender() == Gender.FEMALE).map(user -> user.getAge());
    }

    @Override
    public List<Integer> getAgeList(List<User> users) {
        return getAgeObjects(users).toList().toBlocking().single();
    }

    @Override
    public List<User> getFemaleList(List<User> users) {
        return getFemaleObjects(users).toList().toBlocking().single();
    }

    @Override
    public List<Integer> getFemaleAgeList(List<User> users) {
        // return getAgeList(getFemaleList(users));
        return getFemaleAgeObjects(users).toList().toBlocking().single();
    }

    @Override
    public List<Integer> getFemalAgeList(List<User> users, int limit) {
        return getFemaleAgeObjects(users).take(limit).toList().toBlocking().single();
    }

    @Override
    public List<User> getFemaleList(List<User> users, int limit) {
        return getFemaleObjects(users, limit).toList().toBlocking().single();
    }

}
