package idv.carl.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import idv.carl.model.Gender;
import idv.carl.model.User;

/**
 * @author Carl Lu
 */
public class UserServiceImpl implements UserService {

    @Override
    public List<Integer> getAgeList(List<User> users) {
        List<Integer> ageList = new ArrayList<Integer>();

        for (User user : users) {
            ageList.add(user.getAge());
        }

        return ageList;
    }

    @Override
    public List<User> getFemaleList(List<User> users) {
        List<User> femaleList = users;
        Iterator<User> iterator = users.iterator();

        while (iterator.hasNext()) {
            if (iterator.next().getGender() != Gender.FEMALE) {
                iterator.remove();
            }
        }

        return femaleList;
    }

    @Override
    public List<Integer> getFemaleAgeList(List<User> users) {
        // This will iterate the loop twice.
        // return getAgeList(getFemaleList(users));

        List<Integer> ageList = new ArrayList<Integer>();

        for (User user : users) {
            if (user.getGender() == Gender.FEMALE) {
                ageList.add(user.getAge());
            }
        }

        return ageList;
    }

    @Override
    public List<Integer> getFemalAgeList(List<User> users, int limit) {
        return getAgeList(getFemaleList(users, limit));
    }

    @Override
    public List<User> getFemaleList(List<User> users, int limit) {
        // return getFemaleList(users).subList(0, limit);
        List<User> femaleList = new ArrayList<>();

        int i = 0;
        for (User user : users) {
            if (i >= limit) {
                break;
            }
            if (user.getGender() == Gender.FEMALE) {
                femaleList.add(user);
                i++;
            }

        }

        return femaleList;
    }

}
