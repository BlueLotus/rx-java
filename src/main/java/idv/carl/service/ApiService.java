package idv.carl.service;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;

/**
 * @author Carl Lu
 */
public class ApiService {

    public Observable<List<String>> getCountries() {
        List<String> countries = new ArrayList<>();
        countries.add("USA");
        countries.add("China");
        countries.add("Atlantis");
        return Observable.just(countries);
    }
}
