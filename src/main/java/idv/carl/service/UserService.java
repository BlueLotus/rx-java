package idv.carl.service;

import java.util.List;

import idv.carl.model.User;

/**
 * @author Carl Lu
 */
public interface UserService {

    List<Integer> getAgeList(List<User> users);

    public List<User> getFemaleList(List<User> users);

    List<Integer> getFemaleAgeList(List<User> users);

    List<Integer> getFemalAgeList(List<User> users, int limit);

    List<User> getFemaleList(List<User> users, int limit);

}
