package idv.carl.model;

/**
 * @author Carl Lu
 */
public enum Gender {

    MALE,
    FEMALE;

}
