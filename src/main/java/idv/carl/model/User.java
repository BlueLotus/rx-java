package idv.carl.model;

/**
 * @author Carl Lu
 */
public class User {

    private Long id;
    private Integer age;
    private Gender gender;

    public static User getInstance() {
        User user = new User();
        return user;
    }

    public static User getMaleInstance() {
        User user = User.getInstance();
        user.setGender(Gender.MALE);
        return user;
    }

    public static User getFemaleInstance() {
        User user = User.getInstance();
        user.setGender(Gender.FEMALE);
        return user;
    }

    public void initRandomly(int i) {
        this.id = Long.valueOf(i);
        this.age = Integer.valueOf((int) (Math.random() * 100 + 1));
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
